import os
import requests

TOKEN = os.getenv("TOKEN")
URL = "https://api.github.com/repos/boto/boto3/pulls"
HEADERS = {'Authorization': f'Bearer {TOKEN}'}
PARAMS = dict()
PARAMS["per_page"] = 100


def get_pull_requests(state):
    """
    Example of return:
    [
        {"title": "Add useful stuff", "num": 56, "link": "https://github.com/boto/boto3/pull/56"},
        {"title": "Fix something", "num": 57, "link": "https://github.com/boto/boto3/pull/57"},
    ]
    """
    # print("Token:", TOKEN)
    try:
        response = requests.get(URL, headers=HEADERS, params=PARAMS)
    except Exception as e:
        print("Error getting data:", e)
        return False
    else:
        if response.status_code == 200:
            result = []
            for item in response.json():
                if item["state"] == state:
                    pull = {}
                    pull["num"] = item["number"]
                    pull["title"] = item["title"]
                    pull["link"] = item["html_url"]

                    result.append(pull)
                    del pull
        else:
            print("Response status code:", response.status_code)
            return False

    return result
